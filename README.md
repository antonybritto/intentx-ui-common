# Intentx common components

Repository for shared UI components

## Installation

After cloning the repository, install dependencies:
```sh
cd <project folder>/react-ui-common
npm install
```

Now you can run your local server:
```sh
npm start
```
Server is located at http://localhost:3000
